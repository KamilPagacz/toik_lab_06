package com.company;

/** nalezy zaimplementowac interfejs Quiz. */
class QuizImpl implements Quiz{

    private int digit;

    public QuizImpl() {
        this.digit = 254;    // zmienna moze ulegac zmianie!
    }

    @Override
    public void isCorrectValue(int value) throws ParamTooLarge, ParamTooSmall {
        if(value>this.digit) {
            throw new Quiz.ParamTooLarge();
        }
        if(value<this.digit) {
            throw new Quiz.ParamTooSmall();
        }
    }

}