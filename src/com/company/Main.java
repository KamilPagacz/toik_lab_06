package com.company;

class Main {

    /**
     * metoda main powinna implementowac algorytm do
     * jak najszybszego wyszukiwania wartosci
     * zmiennej digit z klasy QuizImpl (zakladamy ze
     * programista nie zna zawartosci klasy QuizImpl).
     * Nalezy zalozyc, ze pole digit w klasie QuizImpl
     * moze w kazdej chwili ulec zmianie. Do wyszukiwania
     * odpowiedniej wartosci nalezy wykorzystywac tylko
     * i wylacznie metode isCorrectValue - jesli metoda
     * przestanie rzucac wyjatki wowczas mamy pewnosc ze
     * poszukiwana zmienna zostala odnaleziona.
     */

    /**
     * Nie wiem czy prawidłowo zinterpretowałem polecenie zadania.
     * Do zmiennej digit w klasie Main wprowadzam swój strzał.
     * Jeśli jest nieprawidłowy wyłapywany jest odpowiedni wyjątek.
     * Następnie zmieniane są granice a nowemu strzałowi przypisywany jest środek.
     * Taka implementacja pozwala na strzelenie poza przedział (Quiz.MIN_VALUE, Quiz.MAX_VALUE)
     * by i tak na końcu otrzymać rozwiązanie.
     */

    public static void main(String[] args) {

        Quiz quiz = new QuizImpl();
        int digit = 512; // zainicjuj zmienna
        int left = Quiz.MIN_VALUE;
        int right = Quiz.MAX_VALUE;

        for (int counter = 1; ; counter++) {

            try {

                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;

            } catch (Quiz.ParamTooLarge ptm) {
                System.out.println("Argument za duzy!!!");
                // implementacja logiki...
                right = digit;
                digit = (left + right) / 2;
                System.out.println("Nowy zakres: <"+left+","+right+">");
                System.out.println("Nowy strzał: "+digit);
            } catch (Quiz.ParamTooSmall pts) {
                System.out.println("Argument za maly!!!");
                // implementacja logiki...
                left = digit;
                digit = (left + right) / 2;
                System.out.println("Nowy zakres: <"+left+","+right+">");
                System.out.println("Nowy strzał: "+digit);
            }
        }
    }
}